#include <stdio.h> //#include <iostream.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>

/* Unsigned integer types  */
#define uint8_t unsigned char
#define uint16_t unsigned short
#define uint32_t unsigned long

uint8_t int_on = 0 ;
uint32_t Data_ready_count ;
typedef enum {FALSE = 0, TRUE = !FALSE} bool;
//---------------------------------------------------------------------------------------------------------
static void SaveData (int32_t col0, int32_t col1, int32_t col2);

//---------------------------------------------------------------------------------------------------------
//  name: SaveData
//  function:  Take memory data and tranfer to a txt file
//  parameter: int32_t
//  The return value:  NULL*/
//---------------------------------------------------------------------------------------------------------
static void SaveData (int32_t col0, int32_t col1, int32_t col2){
//FILE 0---------------------------------------------------------------------------------------------------
    FILE *datos0 ;                   // necesary to work with txt files
    datos0 = fopen("_data.txt", "a+") ; //open the txt file in writing mode and write after the last line

    if (col0 < 0){
        col0 = -col0 ;
        fprintf(datos0,"-%d \t", col0) ;
    }
    else{
        fprintf(datos0," %d \t", col0) ;
    }

    if (col1 < 0){
        col1 = -col1 ;
        fprintf(datos0,"-%d \t", col1) ;
    }
    else{
        fprintf(datos0," %d \t", col1) ;
    }

    if (col2 < 0){
        col2 = -col2 ;
        fprintf(datos0,"-%d \n", col2) ;
    }
    else{
        fprintf(datos0," %d \n", col2) ;
    }
//---------------------------------------------------------------------------------------------------------
    fclose(datos0) ;
}
//---------------------------------------------------------------------------------------------------------
//  name: delay
//  function:  use the clock function to generate a delay
//  parameter: int
//  The return value:  NULL*/
//---------------------------------------------------------------------------------------------------------
static void delay(int number_of_seconds)
{
    // Converting time into milli_seconds
    int milli_seconds = 1000 * number_of_seconds;

    // Storing start time
    clock_t start_time = clock();

    // looping till required time is not achieved
    while (clock() < start_time + milli_seconds);
}
//MAIN Program---------------------------------------------------------------------------------------------
int main ()
{

    uint32_t size_ch0 = 0 ;
    uint32_t size_ch1 = 0 ;
    uint32_t size_ch2 = 0 ;
    uint32_t datacount ;
    uint32_t datatime ; //ok
    float sample_rate ; //ok
    float sample_rate_per_channel ;
    uint8_t select_sps ; //ok
    uint8_t case_sps = 0 ; //ok


    //set the seed
    srand((unsigned)time(NULL));
//---------------------------------------------------------------------------------------------------------
//SAMPLE RATE MENU---------------------------------------------------------------------------------------------------
    do{
        system("clear");
        printf("\t Samples rate for the ADC\n") ;
        printf("\t*** Choose the sample rate for the acquisition: *** \n") ;
        printf("\t 4000 sps   (0)\n");
        printf("\t 3300 sps   (1)\n");
        printf("\t 2750 sps   (2)\n");
        printf("\t 2000 sps   (3)\n");
        printf("\t 1350 sps   (4)\n");
        printf("\t 800 sps    (5)\n");
        printf("\t 450 sps    (6)\n");
        printf("\t 100 sps    (7)\n");
        printf("\t 60 sps     (8)\n");
        printf("\t 50 sps     (9)\n");
        printf("\t 30 sps     (10)\n");
        printf("\t 25 sps     (11)\n");
        printf("\t 15 sps     (12)\n");
        printf("\t 10 sps     (13)\n");
        printf("\t 5 sps      (14)\n");
        printf("\t 2.5 sps    (15)\n");
        printf("\t EXIT       (16)\n");
        printf("\t------------------------------\n");
        printf("\t *** Enter Your Choice *** \n");
        printf("\t------------------------------\n\n");
        printf("\tOption for the sample rate: ") ;
        scanf("%d", &select_sps) ;
        fflush(stdout);

        switch (select_sps){
            case 0:
                printf("\n\tYou Selected 4000 sps!");
                sample_rate = 4000 ;
                case_sps = 1 ;
                break ;
            case 1:
                printf("\n\tYou Selected 3300 sps!");
                sample_rate = 3300 ;
                case_sps = 1 ;
                break ;
            case 2:
                printf("\n\tYou Selected 2750 sps!");
                sample_rate = 2750 ;
                case_sps = 1 ;
                break ;
            case 3:
                printf("\n\tYou Selected 2000 sps!");
                sample_rate = 2000 ;
                case_sps = 1 ;
                break ;
            case 4:
                printf("\n\tYou Selected 1.350 sps!");
                sample_rate = 1350 ;
                case_sps = 1 ;
                break ;
            case 5:
                printf("\n\tYou Selected 800 sps!");
                sample_rate = 800 ;
                case_sps = 1 ;
                break ;
            case 6:
                printf("\n\tYou Selected 450 sps!");
                sample_rate = 450 ;
                case_sps = 1 ;
                break ;
            case 7:
                printf("\n\tYou Selected 100 sps!");
                sample_rate = 100 ;
                case_sps = 1 ;
                break ;
            case 8:
                printf("\n\tYou Selected 60 sps!");
                sample_rate = 60 ;
                case_sps = 1 ;
                break ;
            case 9:
                printf("\n\tYou Selected 50 sps!");
                sample_rate = 50 ;
                case_sps = 1 ;
                break ;
            case 10:
                printf("\n\tYou Selected 30 sps!");
                sample_rate = 30 ;
                case_sps = 1 ;
                break ;
            case 11:
                printf("\n\tYou Selected 25 sps!");
                sample_rate = 25 ;
                case_sps = 1 ;
                break ;
            case 12:
                printf("\n\tYou Selected 15 sps!");
                sample_rate = 15 ;
                case_sps = 1 ;
                break ;
            case 13:
                printf("\n\tYou Selected 10 sps!");
                sample_rate = 10 ;
                case_sps = 1 ;
                break ;
            case 14:
                printf("\n\tYou Selected 5 sps!");
                sample_rate = 5 ;
                case_sps = 1 ;
                break ;
            case 15:
                printf("\n\tYou Selected 2.5 sps!");
                sample_rate = 2.5 ;
                case_sps = 1 ;
                break ;
            case 16:
                printf("\n\tclosing the program....");
                abort();
            default:
                printf("\n\t\n\nINVALID SELECTION...Please try again\n");
                case_sps = 0 ;
        }
    }while(case_sps!=1) ;
//TIME MENU---------------------------------------------------------------------------------------------------

    printf("\n\tEnter the time in secons for the acquisition: ") ;
    scanf("%ld", &datatime) ;
    sample_rate_per_channel = sample_rate ;
    datacount = datatime * (sample_rate_per_channel) ;
    fflush(stdin) ;
    //pointer for each analog input
    int32_t *ch0 ; int32_t *ch1 ; int32_t *ch2 ;

//--------------------------------------------------------------------------------------------------------
//ch0 memory block-----------------------------------------------------------------------------------------
    ch0 = malloc(sizeof(int32_t) * datacount); /* allocate memory for datacount int's */
    if (!ch0) { /* If data == 0 after the call to malloc, allocation failed for some reason */
        perror("Error allocating memory for channel 0");
        abort();
    }
    /* ch points to a valid block of memory.
     clearing block. */
    memset(ch0, 0.0, sizeof(int32_t)*datacount);

//ch1 memory block-----------------------------------------------------------------------------------------
    ch1 = malloc(sizeof(int32_t) * datacount); /* allocate memory for datacount int's */
    if (!ch1) { /* If data == 0 after the call to malloc, allocation failed for some reason */
        perror("Error allocating memoryfor channel 1");
        abort();
    }
    /* ch points to a valid block of memory.
     clearing block. */
    memset(ch1, 0.0, sizeof(int32_t)*datacount);

//ch2 memory block-----------------------------------------------------------------------------------------
    ch2 = malloc(sizeof(int32_t) * datacount); /* allocate memory for datacount int's */
    if (!ch2) { /* If data == 0 after the call to malloc, allocation failed for some reason */
        perror("Error allocating memory for channel 2");
        abort();
    }
    /* ch points to a valid block of memory.
     clearing block. */
    memset(ch2, 0.0, sizeof(int32_t)*datacount);

//TXT file open--------------------------------------------------------------------------------------------
    FILE *datos0 = NULL;
    datos0 = fopen("_data.txt", "w");
    if (datos0 == NULL){
        printf("Error opening file 0!\n") ;
        exit(1) ;
    }

    printf("\n\t Ready...");

    printf("\n\tAcquiring %ld samples at %f SPS per channel...", datacount, sample_rate_per_channel);



    fflush(stdout) ;
//LOOP-----------------------------------------------------------------------------------------------------

    while(1){

            //data generation
            // generate actual random number
            ch0[size_ch0] = rand() % 1000 ;
            size_ch0++;
            ch1[size_ch1] = rand() % 100 ;
            size_ch1++;
            ch2[size_ch2] = (rand() % 10) / 2 ;
            size_ch2++;

            //out condition
            if(size_ch0 == datacount && size_ch1 == datacount && size_ch2 == datacount) {
                    printf ("\n\tbuffer is full ") ;
                    delay(1000) ;
                    break ;
            }

    }//while

            for (int i = 0; i<size_ch0; i++)
            {
                //printf("\n\t %d", ch0[size_ch0]);
                //printf("\t %d", ch1[size_ch1]);
                //printf("\t %d", ch2[size_ch2]);
                SaveData(ch0[i], ch1[i], ch2[i]);
            }

            fclose(datos0) ;
            free(ch0) ;
            free(ch1) ;
            free(ch2) ;
            printf("\n\tdone\n");
        return 0 ;

}//int main
